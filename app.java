public class app {
    public enum UserType {
        ADMIN, REGULAR
    }
    public static void main (String args[]) {
        
    }
    
    public class User {
        public String username;
        public String password;
        public UserType user;

        public User(String username, String password, UserType user) {
            this.username = username;
            this.password = password;
            this.user = user;
        }

        public String getUsername() {
            return this.username;
        }

        public String getPassword() {
            return this.password;
        }

        public UserType getUser() {
            return this.user;
        }
    }
}